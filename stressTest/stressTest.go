/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package main

import (
	
	A	"git.duniter.org/gerard94/util/alea"
	B	"git.duniter.org/gerard94/skills/base"
	M	"git.duniter.org/gerard94/util/misc"
	R	"git.duniter.org/gerard94/util/resources"
	S	"git.duniter.org/gerard94/util/sets3"
		"crypto/aes"
		"encoding/base64"
		"errors"
		"flag"
		"fmt"
		"encoding/hex"
		"crypto/hmac"
		"os"
		"strings"
		"text/scanner"
		"crypto/sha256"

)

const (
	
	nbElems = 10
	nickLength = 10
	
	seed = 0
	
	wordsPath = "skills/stressTest/wordList.txt"
	basePath = "skills/system/base.data"
	
	keySize = 256 / 8
	keyPath = "skillsclient/managerKey.txt"

)

var (
	
	wordsRsrc = R.FindDir(wordsPath)
	baseRsrc = R.FindDir(basePath)
	
	keyP = R.FindDir(keyPath)
	manKey string
	key []byte

)

var (
	
	words *S.Set

)

func getMKeys (key1, key2 *[]byte) bool {
	
	get1K := func (s *scanner.Scanner) []byte {
		s.Scan()
		ss := s.TokenText()
		M.Assert(ss[0] == '"' && ss[len(ss) - 1] == '"', ss, 101)
		k, err := hex.DecodeString(ss[1:len(ss) - 1]); M.Assert(err == nil, err, 100)
		return k
	}
	
	f, err := os.Open(keyP)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Incorrect path to Manager Key")
		return false
	}
	defer f.Close()
	s := new(scanner.Scanner)
	s.Init(f)
	s.Error = func(s *scanner.Scanner, msg string) {M.Halt(errors.New("File " + keyP + " incorrect"), 100)}
	s.Mode = scanner.ScanStrings
	k := get1K(s)
	M.Assert(len(k) == keySize, "key has wrong length:", k, 103)
	*key1 = k
	k = get1K(s)
	*key2 = k
	return true
}

func getKey () bool {
	
	equal := func (k1, k2 []byte) bool {
		if len(k1) != len(k2) {
			return false
		}
		for i := range k1 {
			if k1[i] != k2[i] {
				return false
			}
		}
		return true
	}
	
	if manKey == "" {
		flag.PrintDefaults()
		return false
	}
	
	var mk, mkh []byte
	if !getMKeys(&mk, &mkh) {
		return false
	}
	
	h := sha256.New()
	h.Write([]byte(manKey))
	mh := h.Sum(nil)
	M.Assert(len(mh) == keySize, len(mh), 100)
	
	b, err := aes.NewCipher(mh); M.Assert(err == nil, err, 101)
	bSize := b.BlockSize()
	M.Assert(keySize % bSize == 0 && keySize / bSize > 0, "bSize =", bSize)
	key = make([]byte, keySize)
	for i := 0; i < keySize; i += bSize {
		b.Decrypt(key[i: i + bSize], mk[i: i + bSize])
	}
	
	h.Reset()
	h.Write([]byte(key))
	kh := h.Sum(nil)
	
	if !equal(kh, mkh) {
		fmt.Fprintln(os.Stderr, "Error: wrong key")
		return false
	}
	return true
}

func cryptPass (pass string) string {
	h := hmac.New(sha256.New, key)
	h.Write([]byte(pass))
	return base64.RawStdEncoding.EncodeToString(h.Sum(nil))
}

func nw () string {
	w := words.ElemOfRank(int(A.IntRand(0, int64(words.NbElems()))))
	return w
}

func nwex () string {
	w := words.ElemOfRank(int(A.IntRand(0, int64(words.NbElems()))))
	words.Excl(w)
	return w
}

func ne () int {
	return int(A.IntRand(0, nbElems))
}

func nb () bool {
	return A.IntRand(0, 2) == 0
}

func nN () string {
	var b strings.Builder
	for i := 0; i < nickLength; i++ {
		b.WriteRune(rune(A.IntRand('a', 'z' + 1)))
	}
	return b.String()
}

func main () {
	if !getKey() {
		return
	}
	
	os.Remove(baseRsrc)
	fmt.Println("Starting")
	B.OpenBase()
	defer B.CloseBase()
	
	wordsCopy := words.Copy()
	fmt.Println("Locations")
	for i := 0; i < nbElems; i++ {
		co := nwex()
		for j := 0; j < nbElems; j++ {
			rg := nwex()
			_, err := B.InsertLocation(co, rg); M.Assert(err == nil, err, 100)
			fmt.Println(i, j)
		}
	}
	words = wordsCopy.Copy()
	fmt.Println("Services")
	for i := 0; i < nbElems; i++ {
		ca := nwex()
		for j := 0; j < nbElems; j++ {
			sp := nwex()
			_, err := B.InsertService(ca, sp); M.Assert(err == nil, err, 101)
			fmt.Println(i, j)
		}
	}
	
	fmt.Println("Identities")
	locs, _ := B.AllLocations("")
	sers, _ := B.AllServices("")
	rgNb := nbElems * nbElems
	m := 0
	for i := 0; i < rgNb; i++ {
		words = wordsCopy.Copy()
		ico := ne(); irg := ne()
		co := locs[ico].Country; rg := locs[ico].Regions[irg]
		for j := 0; j < nbElems; j++ {
			var cys []string
			switch A.IntRand(0, 4) {
			case 0:
				cys = []string{}
			case 1:
				cys = []string{nwex()}
			case 2:
				cys = []string{nwex(), nwex()}
			case 3:
				cys = []string{nwex(), nwex(), nwex()}
			}
			loc := B.LocationListT{&B.LocationT{co, []*B.RegionT{&B.RegionT{rg, cys}}}}
			for k := 0; k < nbElems; k++ {
				nick := nN()
				for {
					id, err := B.IdentityOf(nick)
					if err != nil || id == nil {break}
					nick = nN()
				}
				isuca := ne(); isusp :=ne()
				suca := sers[isuca].Category; susp := sers[isuca].Specialities[isusp]
				iseca := ne(); isesp :=ne()
				seca := sers[iseca].Category; sesp := sers[iseca].Specialities[isesp]
				id := &B.IdentityT{
					nick,
					cryptPass(nw()),
					nw(),
					B.PubkeyT(nw()),
					nw(),
					loc,
					nb(),
					nb(),
					B.ServiceListT{&B.ServiceT{suca, []string{susp}}},
					B.ServiceListT{&B.ServiceT{seca, []string{sesp}}},
					nw(),
				}
				_, err := B.InsertIdentity(id);
				if err != nil {
					fmt.Println("err =", err, "id.Nickname =", id.Nickname)
				}
				M.Assert(err == nil, err, 100)
				m++
				if m % 10 == 0 {
					fmt.Println(m)
				}
			}
		}
	}
}

func init () {
	words = S.NewSet()
	f, err := os.Open(wordsRsrc)
	M.Assert(err == nil, err, 100)
	defer f.Close()
	s := new(scanner.Scanner)
	s.Init(f)
	s.Error = func(s *scanner.Scanner, msg string) {M.Halt(errors.New(msg), 101)}
	s.Mode = scanner.ScanIdents
	for s.Scan() != scanner.EOF {
		words.Incl(s.TokenText())
	}
	flag.StringVar(&manKey, "k", "", "Private Key")
	flag.Parse()
	if seed >= 0 {
		A.Randomize(seed)
	}
}
