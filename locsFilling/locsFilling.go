/* 
Skills

Copyright (C) 2023 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

package main

import (
	
	B	"git.duniter.org/gerard94/skills/base"
	C	"git.duniter.org/gerard94/util/csv"
	M	"git.duniter.org/gerard94/util/misc"
	R	"git.duniter.org/gerard94/util/resources"
		"fmt"

)

const (
	
	dataPath = "skills/locsFilling/locs.csv"

)

var (
	
	rsrcDataPath = R.FindDir(dataPath)

)

func main () {
	csv := C.CompileFile(rsrcDataPath)
	if csv == nil {
		fmt.Println("ERROR in csv")
		return
	}
	B.OpenBase()
	defer B.CloseBase()
	var loc [2]string
	for _, l := range csv {
		if l[0] == "" && l[1] == "" {
			continue
		}
		for ci := 0; ci < 2; ci++ {
			if l[ci] != "" {
				ll := l[ci]
				if ll[0] == '"' {
					M.Assert(ll[len(ll) - 1] == '"', ll, 100)
					loc[ci] = string([]byte(ll)[1:len(ll) - 1])
				} else {
					loc[ci] = ll
				}
			}
		}
		B.InsertLocation(loc[0], loc[1])
	}
}
