query ListLocations ($co: String, $rg: String) {
	locationList(country: $co, region: $rg) {
		country
		region
		city
	}
}

{
}
{
	"co":
		"Espagne"
}
{
	"co":
		"France",
	"rg":
		"Val de Marne"
}
