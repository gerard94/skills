query ListIdentities ($co: String, $rg: String, $cy: String, $se: [ServiceQ_input!], $ss: SearchedOrSupplied) {
	identityList(country: $co, region: $rg, city: $cy, services: $se, searchedOrSupplied: $ss) {
		nickname
		password
		g1_nickname
		pubkey
		email
		locations {
			country
			regions {
				region
				cities
			}
		}
		transport
		pickup
		supplied_services {
			...Services
		}
		searched_services {
			...Services
		}
		additional_informations
	}
}
fragment Services on Service {
	category
	specialities
}


{
}

{
	"co":
		"France",
	"rg":
		"Seine et Marne"
}

{
	"se":
		{
			"category":
				"Informatique"
		}
}
