module git.duniter.org/gerard94/skills

go 1.20

require git.duniter.org/gerard94/util v1.0.3

replace git.duniter.org/gerard94/util => ../../util
